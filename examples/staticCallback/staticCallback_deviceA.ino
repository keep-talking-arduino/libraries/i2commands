/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>
#include <CommandHandler.h>

/* Create a variable of the CommandHandler. */
CommandHandler *_cmdHandler;

/* This is the function that will be called whenever this device receives a command from another device. 
In this case we will print the incomming command and also extract the vallue that has been passed along with it. */
void processCommand(Command* cmd){
    Serial.print("RAW command: ");
    Serial.println(cmd->toString());

    if(cmd->getCmdCode() == CMD_PRINT_NUMBER)
    {
            Serial.print("Received number: ");
            /* Values are always received as String, so we'll have to convert it to an integer. */
            int receivedNumber = cmd->getValue().toInt();
            Serial.println(receivedNumber);
    }
    else if(cmd->getCmdCode() == CMD_PRINT_GREETING)
    {
        Serial.print("Hello, ");
        Serial.print(cmd->getValue());
        Serial.println("!");
    }
    
    Serial.println("\n");
}

void setup()
{
Serial.begin(9600);

/* The CommandHandler is a singleton that you can just ask to pass it's instance wherever you need it.
In this case we'll bind it tho the _cmdHandler variable. */
_cmdHandler = CommandHandler::getInstance();

/* To configure the CommandHandler, we will pass it the i2c-address that this device will listen on.
In this case it is 1 but you can configure it any way you like. 
The second argument refers to the function that you will use to process any command received by this device. */
_cmdHandler->configure(ADDRESS_DEVICE_A, processCommand);

Serial.println("Setup complete(Device A)!");
}

void loop()
{
  /* Every time loop() gets executed, pending commands will be transmitted to their intended recipients. 
  This is handled by the run() method of the CommandHandler. */
    _cmdHandler->run();
}

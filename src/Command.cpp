/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Command.h>
#include <CommandHandler.h>

/*********************************************************************
 * Creates command from parameters, sender is set to current device-address.
*********************************************************************/
Command::Command(int recipient, int action, String value = ""){
  _sender = CURRENT_DEVICE_ADDRESS;
  _recipient = recipient;
  _cmdCode = action;
  _value = value;
}

/*********************************************************************
 * Creates command from string.
 * Only used for parsing incomming commands.
*********************************************************************/
Command::Command(String command){
  _sender = getSection(1, command).toInt();
  _recipient = CURRENT_DEVICE_ADDRESS;
  _cmdCode = getSection(2, command).toInt();
  _value = getSection(3, command);
}

int Command::getSender(){ return _sender; }
int Command::getRecipient(){ return _recipient; }
int Command::getCmdCode(){ return _cmdCode; }
String Command::getValue(){ return _value; }

/*********************************************************************
 * Concatenates properties to one single string for sending.
*********************************************************************/
String Command::toString(){
  String returnString = "|" + String(_sender) + "|" + String(_cmdCode) + "|" + _value + "|";
  if(returnString.length() > 32){
    return returnString.substring(0, 31) + "|";
  }
  return returnString;
}

/*********************************************************************
 * Returns desired value of command from a string.
*********************************************************************/
String Command::getSection(int section, String command, char seperator){
  int startPos = 1 + getNthIndex(command, seperator, section);
  int endPos =  getNthIndex(command, seperator, section + 1);

  // if not exists leave empty
  if(endPos == -1){
    return F("ERR");
  }

  return command.substring(startPos, endPos);
}

/*********************************************************************
 * Extract value from section of command seperated by a vetical bar "|".
*********************************************************************/
String Command::getSection(int section, String command){
  return getSection(section, command, '|');
}

/*********************************************************************
 * Extract value of string where values are seperated by commas.
*********************************************************************/
String Command::getValueOnIndex(int index){
  String tmpValue = "," + _value + ",";
  return getSection(index, tmpValue, ',');
}

/*********************************************************************
 * Get position of given charachter on the n'th occurrence.
*********************************************************************/
int Command::getNthIndex(String s, char t, int n)
{
    int count = 0;
    for (unsigned int i = 0; i < s.length(); i++)
    {
        if (s[i] == t)
        {
            count++;
            if (count == n)
            {
                return i;
            }
        }
    }
    return -1;
}

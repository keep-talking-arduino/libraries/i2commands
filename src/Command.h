/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef Command_H
#define Command_H

#include <Arduino.h>

class Command {
  public:
    Command(int recipient, int action, String value);
    Command(String cmd);
    int getSender();
    int getRecipient();
    int getCmdCode();
    String getValue();
    String getValueOnIndex(int index);
    String toString();
  private:
    int _sender;
    int _recipient;
    int _cmdCode;
    String _value;

    static String getSection(int section, String string);
    static String getSection(int section, String string, char seperator);
    static int getNthIndex(String s, char t, int n);
};

#endif

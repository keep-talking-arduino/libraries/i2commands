/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CommandBuffer.h"

/*********************************************************************
 * Returns the first free slot of the buffer.
 * If the buffer is full, -1 will be returned.
*********************************************************************/
int CommandBuffer::findFirstFreeSlot()
{
  for (int index = 0; index < BUFFER_SIZE; index++)
  {
    if (_commandBuffer[index] == NULL)
    {
      return index;
    }
  }
  return -1; // none found
}

/*********************************************************************
 * Adds new command to buffer.
*********************************************************************/
void CommandBuffer::add(Command *cmd)
{
  int freeSlot = findFirstFreeSlot();
  if (freeSlot < 0)
  { // If buffer is full
    Serial.println(F("Err: CmdBuffer full!"));
    return;
  }

  _commandBuffer[freeSlot] = cmd;
}

/*********************************************************************
 * Empties the buffer by calling remove for every slot.
*********************************************************************/
void CommandBuffer::clear()
{
  for (int index = 0; index < BUFFER_SIZE; index++)
  {
    remove(index);
  }
}

/*********************************************************************
 * Destructs object and removes reference to Command from buffer.
*********************************************************************/
void CommandBuffer::remove(int index)
{
  delete _commandBuffer[index];
  _commandBuffer[index] = NULL;
}

/*********************************************************************
 * Returns value in buffer on given index.
*********************************************************************/
Command *CommandBuffer::get(int index)
{
  return _commandBuffer[index];
}

int CommandBuffer::size()
{
  return BUFFER_SIZE;
}

/*********************************************************************
 * Returns true if all elements in buffer are NULL.
*********************************************************************/
bool CommandBuffer::isEmpty()
{
  for (int index = 0; index < BUFFER_SIZE; index++)
  {
    if (_commandBuffer[index] != NULL)
    {
      return false;
    }
  }
  return true;
}

/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef Settings_H
#define Settings_H

#define ADDRESS_DEVICE_A 1
#define ADDRESS_DEVICE_B 2

#define CMD_PRINT_NUMBER 301
#define CMD_PRINT_GREETING 302

#endif
